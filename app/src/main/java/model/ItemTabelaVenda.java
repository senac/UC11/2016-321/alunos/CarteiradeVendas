package model;

/**
 * Created by sala304b on 22/09/2017.
 */

import java.text.NumberFormat;

public class ItemTabelaVenda {


    private Produtos produtos;
    private double preco;
    private NumberFormat nf = NumberFormat.getCurrencyInstance();


    public ItemTabelaVenda() {

    }

    public ItemTabelaVenda(Produtos produtos, double preco) {
        this.produtos = produtos;
        this.preco = preco;
    }

    public Produtos getProduto() {
        return produtos;
    }

    public void setProduto(Produtos produtos) {
        this.produtos = produtos;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return this.produtos.getNome() + " - " + nf.format(this.getPreco());
    }

    public ItemAtendimento ConvertToItemAtendimento() {
        return new ItemAtendimento(this.getProduto(), 1, this.getPreco());
    }




}
