package model;

import java.io.Serializable;

/**
 * Created by sala304b on 22/09/2017.
 */

public class Produtos implements Serializable {

    private int codigo;
    private String nome;

    public Produtos() {
    }

    public Produtos(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}