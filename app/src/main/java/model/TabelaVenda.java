package model;

/**
 * Created by sala304b on 22/09/2017.
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TabelaVenda {

    private Date vigenciaInicio ;
    private Date vingenciaFim ;
    private List<ItemTabelaVenda> itens  = new ArrayList<>() ;


    public TabelaVenda() {

    }

    public TabelaVenda(Date vigenciaInicio, Date vingenciaFim) {
        this.vigenciaInicio = vigenciaInicio;
        this.vingenciaFim = vingenciaFim;
    }

    public Date getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(Date vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public Date getVingenciaFim() {
        return vingenciaFim;
    }

    public void setVingenciaFim(Date vingenciaFim) {
        this.vingenciaFim = vingenciaFim;
    }

    public List<ItemTabelaVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemTabelaVenda> itens) {
        this.itens = itens;
    }


    public void add(ItemTabelaVenda item) {
        this.itens.add(item);
    }

    public void remove(ItemTabelaVenda item){
        this.itens.remove(item);
    }

}
