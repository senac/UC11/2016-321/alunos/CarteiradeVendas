package model;

/**
 * Created by sala304b on 22/09/2017.
 */

import java.io.Serializable;
import java.text.NumberFormat;

public class ItemAtendimento implements Serializable{

    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    private Produtos produtos;
    private int quantidade;
    private double preco;

    public ItemAtendimento() {
    }

    public ItemAtendimento(Produtos produtos, int quantidade, double preco) {
        this.produtos = produtos;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public Produtos getProdutos() {
        return produtos;
    }

    public void setProdutos(Produtos produto) {
        this.produtos = produtos;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public String getPrecoFormatado(){
        return nf.format(this.preco) ;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getTotal() {
        return this.quantidade * this.preco;
    }

    public String getTotalFormatado(){
        return nf.format(this.getTotal());
    }
}

