package br.com.senac.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class CadastroClienteActivity extends AppCompatActivity {

    private EditText txt_nome;
    private EditText txt_telefone;
    private EditText txt_email;
    private EditText txt_endereco;
    private EditText txt_numero;
    private EditText txt_bairro;
    private EditText txt_cidade;
    private EditText txt_uf;
    private EditText txt_cep;
    private EditText txt_cpf;



    private Spinner spn_telefone;
    private Spinner spn_email;
    private Spinner spn_endereco;

    private ArrayAdapter <String>adapterTipoEmail;
    private ArrayAdapter <String>adapterTipoEndereco;
    private ArrayAdapter <String>adapterTipoTelefone;
    private ArrayAdapter <String>adapterTipoCpfeCnpj;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_cliente);

        txt_nome = (EditText) findViewById(R.id.txt_nome);
        txt_telefone = (EditText) findViewById(R.id.txt_telefone);
        txt_email = (EditText) findViewById(R.id.txt_email);
        txt_endereco = (EditText) findViewById(R.id.txt_endereco);
        txt_numero = (EditText) findViewById(R.id.txt_numero);
        txt_bairro = (EditText)findViewById(R.id.txt_bairro);
        txt_cidade = (EditText)findViewById(R.id.txt_cidade);
        txt_uf = (EditText) findViewById(R.id.txt_uf);
        txt_cep = (EditText) findViewById(R.id.txt_cep);
        txt_cpf = (EditText) findViewById(R.id.txt_cpf);



        spn_email = (Spinner)findViewById(R.id.spn_email);
        spn_telefone = (Spinner)findViewById(R.id.spn_telefone);


        adapterTipoEmail = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapterTipoEmail.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapterTipoTelefone = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapterTipoTelefone.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn_email.setAdapter(adapterTipoEmail);
        spn_telefone.setAdapter(adapterTipoTelefone);

        adapterTipoEmail.add("Pessoal");
        adapterTipoEmail.add("Comercial");

        adapterTipoTelefone.add("Celular");
        adapterTipoTelefone.add("Fixo");
        adapterTipoTelefone.add("Trabalho");



    }
}
