package br.com.senac.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.ItemAtendimento;
import model.ItemTabelaVenda;
import model.Produtos;
import model.TabelaVenda;

public class VendasActivity extends AppCompatActivity {
    public static final String LISTA = "lista";

    private TabelaVenda tabelaVenda = new TabelaVenda();

    private ListView listViewTabelaVenda;

    private ArrayAdapter<ItemTabelaVenda> adapter;

    private List<ItemAtendimento> listaItensSelecionados = new ArrayList<>();

    public VendasActivity() {
        super();
        initTabelaVenda();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendas);

        int layout = android.R.layout.simple_list_item_multiple_choice;

        listViewTabelaVenda = (ListView) findViewById(R.id.listaTabelaVenda);

        adapter = new ArrayAdapter<ItemTabelaVenda>(this, layout, tabelaVenda.getItens());

        listViewTabelaVenda.setAdapter(adapter);

    }

    private void preencherLista() {

        listaItensSelecionados.clear();

        // pegar itens selecionados na listview
        SparseBooleanArray checados = listViewTabelaVenda.getCheckedItemPositions();
        //percorrer a lista de posicoes pegando os itens no adapter e
        // criar novo itens e colocalos na lista de selecionados
        for (int i = 0; i < checados.size(); i++) {
            ItemAtendimento novoItem = adapter.getItem(i).ConvertToItemAtendimento();
            listaItensSelecionados.add(novoItem);

            /*
            ItemTabelaVenda itemTabelaVenda =  adapter.getItem(i);
            ItemAtendimento novoItem = new ItemAtendimento(itemTabelaVenda.getProdutos() , 1 , itemTabelaVenda.getPreco());
            */
            /*ItemAtendimento novoItem =  new ItemAtendimento(
                    adapter.getItem(i).getProdutos() ,
                    1 ,
                    adapter.getItem(i).getPreco()
            );*/
        }

    }

    @Override
    public void onBackPressed() {

        //alertar usuario


        if(listViewTabelaVenda.getCheckedItemPositions().size() > 0 ) {
            new AlertDialog.Builder(this)
                    .setTitle("Alerta")
                    .setMessage("Deseja incluir os itens selecionados ?")
                    .setCancelable(false) // se clicar back button nao fecha o dialog
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() { //caso ok add itens
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            concluir();
                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() { //caso contrario so volta a tela anterior
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .show();
        }else{
            finish();
        }






    }

    private void concluir() {
        Intent intent = new Intent();
        this.preencherLista();
        intent.putExtra(VendasActivity.LISTA,
                (Serializable) listaItensSelecionados); /// colcar os dados
        setResult(RESULT_OK, intent);

        finish();
    }

    public void salvarItens(View view) {

        concluir();

    }

    private void initTabelaVenda() {
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Bala Go Jelly Chaves 250g - Freegells"), 3));
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Biscoito ChocoCreme 260g - Milka"), 2.75));
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Chocolate Choc&Choc 150g - Milka"), 2.75));
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Bala 7 Belo Framboesa 600g - Arcor"), 2.80));
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Pão de Alho Caseiro (5 unidades)"), 11));
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Refrigerante"), 4.00));
        tabelaVenda.add(new ItemTabelaVenda(new Produtos(1, "Cerveja"), 5));
    }
}
