package br.com.senac.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageViewCadastroCliente  = (ImageView) findViewById(R.id.cadastro_cliente);


        imageViewCadastroCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this , CadastroClienteActivity.class) ;
                startActivity(intent);

            }
        });

        /*

        ImageView imageViewPesquisar  = (ImageView) findViewById(R.id.pesquisar_cliente);


        imageViewPesquisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this , PesquisarClienteActivity.class) ;
                startActivity(intent);

            }
        });
        */


    }

    public void pesquisarCliente(View view){
        Intent intent = new Intent(MainActivity.this , PesquisarClienteActivity.class) ;
        startActivity(intent);
    }

    public void estoque(View view){
        Intent intent = new Intent(MainActivity.this, EstoqueActivity.class);
        startActivity(intent);

    }

    public void vendas(View view){
        Intent intent = new Intent(MainActivity.this, VendasActivity.class);
        startActivity(intent);

    }

    public void parcelas_atraso(View view){
        Intent intent = new Intent(MainActivity.this, Parcelas_AtrasoActivity.class);
        startActivity(intent);

    }

    public void parcelas_receber(View view){
        Intent intent = new Intent(MainActivity.this, Parcelas_receberActivity.class);
        startActivity(intent);

    }

    public void vendas_mes(View view){
        Intent intent = new Intent(MainActivity.this, Vendas_MesActivity.class);
        startActivity(intent);

    }

    public void vendas_periodo(View view){
        Intent intent = new Intent(MainActivity.this, Vendas_periodoActivity.class);
        startActivity(intent);

    }

    public  void btn_adicionar(View view){
        Intent intent = new Intent(MainActivity.this, CadastroClienteActivity.class);
        startActivity(intent);

    }



}
